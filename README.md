# BLE Numpad EDA
This repository contains the schematic and PCB layout for a Bluetooth Low Energy numpad.

### Some notable features include:
* Estimated 1 year or more battery life with rechargeable 150mAh Li battery
* Efficiently driven, dimmable RGB indicator LED
* Full interrupt driven key matrix with no ghosting (NKRO)
* Cherry MX mechanical switches
* Easily add NFC or capacitive proximity sensing with just a PCB trace

### Layout Notes
The RF layout copies the reference design from Nordic's nrf52832 reference design.

![3d rendering](nrfUmpad.png)
![3d rendering](nrfUmpad_back.png)
